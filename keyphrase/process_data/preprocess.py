import numpy
import nltk
from nltk.corpus import stopwords
# nltk.download('stopwords')
from nltk.tokenize import word_tokenize
import regex as re
from keyphrase import constant


def remove_stopword(text):
    text_tokens = word_tokenize(text)
    tokens_without_sw = [word for word in text_tokens if not word in stopwords.words()]
    return " ".join(tokens_without_sw)


def replace_number(text, digit):
    return re.sub("\d+", digit, text)


def remove_punc(text):
    text = re.sub(r"[-()\"#/@;:<>{}`+=~|,]", "", text)
    text = re.sub("\\!\\?", ".", text)
    text = re.sub("\\.+", ".", text)
    text = re.sub(' +', ' ', text)
    return text


def remove_all_punc(text):
    text = re.sub(r"[-()\"#/@;:<>{}`+=~|,].?!", "", text)
    text = re.sub(' +', '', text)
    return text


def clean_text(text):
    text = remove_stopword(text)
    text = remove_punc(text)
    text = replace_number(text, constant.DIGIT)
    return text
