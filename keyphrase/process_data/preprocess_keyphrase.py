from keyphrase import constant
from keyphrase.process_data.preprocess import remove_all_punc
from keyphrase.utility.utilities import read_config
import keyphrase.constant


def separate_present_absent_keyphrase_from_original(text, keyphrase):
    present_keyphrase = []
    absent_keyphrase = []

    text = remove_all_punc(text)
    text_ = text.lower()
    for key in keyphrase:
        key_ = remove_all_punc(key.lower())
        if str(text_).__contains__(str(key_)):
            present_keyphrase.append(key)
        else:
            absent_keyphrase.append(key)

    return present_keyphrase, absent_keyphrase


def order_and_concatenate_keyphrase(text: str, keyphrases: list, type_order: int = 1):
    """
    Order keyphrase and concatenated
    @param type_order: default = 0 :
      - type = 0 : not order the present keyphrase
      - type = 1 : sorted the present keyphrase by their order of the first occurrence in the source text and appended the absent keyphrases to the end
    """
    if len(keyphrases) > 0:
        present_keyphrase, absent_keyphrase = separate_present_absent_keyphrase_from_original(text, keyphrases)

        if type_order == 0:
            return constant.SEP.join(present_keyphrase + absent_keyphrase) + constant.EOS

        elif type_order == 1:
            index_present_key = {}
            for key in present_keyphrase:
                index = text.lower().find(key.lower().strip())
                index_present_key.update({
                    key: index
                })

            order_key = {k: v for k, v in sorted(index_present_key.items(), key=lambda item: item[1])}
            present_keyphrase_ordered = list(order_key.keys())

            return constant.SEP.join(present_keyphrase_ordered + absent_keyphrase) + constant.EOS

    return None
