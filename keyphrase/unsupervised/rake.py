from rake_nltk import Rake
import nltk


class RAKE(object):
    def __init__(self, num_key=2):
        self.rake = Rake()
        self.num_key = num_key

    def extract_keyword_from_text(self, text):
        keywords = set()
        self.rake = Rake()
        self.rake.extract_keywords_from_text(text)
        keys = self.rake.get_ranked_phrases()

        for phrase in keys:
            keywords.add(phrase)
            if len(keywords) == self.num_key:
                break

        return keywords


# text = "Compatibility of systems of linear constraints over the set of natural numbers." \
#        " Criteria of compatibility of a system of linear Diophantine equations, strict" \
#        " inequations, and nonstrict inequations are considered. Upper bounds for components of" \
#        " a minimal set of solutions and algorithms of construction of minimal generating sets" \
#        " of solutions for all types of systems are given. These criteria and the corresponding a" \
#        "lgorithms for constructing a minimal supporting set of solutions can be used in solving all t" \
#        "he considered types systems and systems of mixed types."