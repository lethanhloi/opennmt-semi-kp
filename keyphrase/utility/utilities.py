import random

import pandas as pd
from configparser import ConfigParser
import json


def read_config():
    config_object = ConfigParser()
    config_object.read("config.ini")
    config = config_object["CHARACTERS"]

    return config


def read_data_from_path(path: str, limit: int = -1):
    result = []
    with open(path) as file:
        for index, line in enumerate(file):
            if index == limit:
                break
            result.append(json.loads(line))

    return result


def write_json_to_file(data, path):
    if isinstance(data, list) :
        for doc in data:
            with open(path, "w+") as outfile:
                json.dump(doc, outfile, ensure_ascii=False, indent=4)
    else:
        with open(path, "w+") as outfile:
            json.dump(data, outfile, ensure_ascii=False, indent=4)


def shuffle_data_labeled_and_synthetic(data_labeled, synthetic_data):
    result = data_labeled + synthetic_data
    random.shuffle(result)

    return result
