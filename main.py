import json

from keyphrase.process_data.preprocess_keyphrase import order_and_concatenate_keyphrase
from keyphrase.process_data.preprocess import clean_text
from keyphrase.unsupervised.textrank import TextRank
from keyphrase.utility.utilities import read_data_from_path, read_config, write_json_to_file
import argparse


def generate_data_for_training():
    pass


def synthetic_keyphrase():
    """
    return pseduo_labeled for unlabeled_data by unsupervised_learning or self-learning
    """
    result = []
    text_rank = TextRank()
    data_unlabeled = read_data_from_path(args.input_path_unlabeled_data)
    f_output = open(args.output_path_synthetic_data, "w+")

    for document in data_unlabeled:
        text = document['title'] + ". " + document['abstract']
        processed_text = clean_text(text)
        pseduo_labeld = text_rank.extract_keyword(processed_text)
        pseduo_labeld_concat = order_and_concatenate_keyphrase(text, pseduo_labeld, type_order=1)
        print(pseduo_labeld_concat)
        document["pseduo_keyphrase"] = pseduo_labeld_concat
        f_output.write(json.dumps(document) + "\n")
        result.append(document)

    f_output.close()

    return result


if __name__ == '__main__':
    # config = read_config()

    parser = argparse.ArgumentParser()
    parser.add_argument("-input_path_unlabeled_data",
                        default="data/kp20k/unlabeled/kp20k_unlabel_sample.json")
    parser.add_argument("-output_path_synthetic_data",
                        default="data/kp20k/synthetic_keyphrase/kp20k_unlabel_sample_synthetic.json")
    args = parser.parse_args()

    synthetic_keyphrase()
